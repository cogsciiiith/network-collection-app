import api from "./helper";

export default {
    async getQuestionnaire(qId) {
        return await api
            .get(`/api/core/questions/${qId}`)
            .then(res => res.data)
            .then(questionnaire => {
                if (!questionnaire.success) throw new Error(questionnaire.error);
                return questionnaire.data;
            })
            .catch(err => {
                throw err.isAxiosError ? err.response.data : err;
            });
    }
};