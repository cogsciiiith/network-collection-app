import api from "./helper";

export default {
    async getState(qId) {
        return await api
            .get(`/api/core/state/${qId}`)
            .then(res => res.data)
            .then(state => {
                if (!state.success) throw new Error(state.error);
                return state.data;
            })
    },
    async saveState({ qId, qState }) {
        return await api
            .post(`/api/core/state/${qId}`, {
                body: { state: qState }
            })
            .then(res => res.data)
            .then(newState => {
                if (!newState.success) throw new Error(newState.error);
                return newState.state;
            })
            .catch(err => {
                throw err.isAxiosError ? err.response.data : err;
            });
    },
    async login(ticket) {
        return await api
            .get(`/api/core/login?ticket=${ticket}`, {
                json: false
            })
            .then(res => res.data)
            .then(token => {
                if (!token.success) throw new Error(token.error);
                return token;
            })
            .catch(err => {
                throw err.isAxiosError ? err.response.data : err;
            });
    }
};