import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import('../views/Home.vue'),
    },
    {
        path: '/intra',
        name: 'Intra',
        component: () => import('../views/Intra.vue'),
        beforeEnter: (to, from, next) => {
            store.state.isAuthenticated ? next() : next({ name: 'Home' });
        }
    },
    {
        path: '/inter',
        name: 'Inter',
        component: () => import('../views/Inter.vue'),
        beforeEnter: (to, from, next) => {
            store.state.isAuthenticated ? next() : next({ name: 'Home' });
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('../components/Login.vue'),
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router;
