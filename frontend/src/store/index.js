import Vue from "vue";
import Vuex from "vuex";
import rootActions from "./actions";
import rootMutaions from "./mutations";
import user from "./modules/user";
import questions from "./modules/questions";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
    // root level state
    state: {
        token: null,
        isAuthenticated: false,
        loading: false
    },
    actions: rootActions,
    mutations: rootMutaions,
    modules: {
        user,
        questions
    },
    strict: debug,
    plugins: []
});