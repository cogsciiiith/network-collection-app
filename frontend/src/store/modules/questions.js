import questionsApi from "../../api/questions";

const state = () => ({
    data: {}
});

const getters = {
    getQuestionnaireById: state => id => state.data[id],
    getIntra: state => () => {
        Object.filter = (obj, predicate) =>
            Object.fromEntries(Object.entries(obj).filter(predicate));
        // eslint-disable-next-line no-unused-vars
        return Object.filter(state.data, ([_id, questionnaire]) => questionnaire.type === 'intra');

    },
    getInter: state => () => {
        Object.filter = (obj, predicate) =>
            Object.fromEntries(Object.entries(obj).filter(predicate));
        // eslint-disable-next-line no-unused-vars
        return Object.filter(state.data, ([_id, questionnaire]) => questionnaire.type === 'inter');

    }
};

const actions = {
    async getQuestionnaire({ commit }, qId) {
        await questionsApi
            .getQuestionnaire(qId)
            .then(questionnaire => {
                commit("ADD_QUESTIONNAIRE", { questionnaire });
            })
            .catch(err => {
                console.error(err);
            });
    }
};

const mutations = {
    ADD_QUESTIONNAIRE: (state, { questionnaire }) => {
        state.data = {
            [questionnaire._id]: {
                ...questionnaire
            },
            ...state.data
        };
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};