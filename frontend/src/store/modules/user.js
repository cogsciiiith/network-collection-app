import Vue from 'vue';
import usersApi from "../../api/users";

const state = () => ({
    ssq6: []
});

const getters = {
    getState: (state) => (qId) => {
        return state[qId];
    },
    getSSQ6: (state) => {
        let data = {};
        state.ssq6.forEach(quesId => data[quesId] = state[quesId]);
        return data;
    }
};

const actions = {
    async getState({ commit }, qId) {
        await usersApi
            .getState(qId)
            .then(state => {
                commit("ADD_STATE", { qId, qState: state.state });
            })
            .catch(err => console.error(err));
    },
    async saveState({ commit }, { qId, qState }) {
        await usersApi
            .saveState({ qId, qState })
            .then(state => {
                commit("ADD_STATE", { qId, qState: state.state });
            })
            .catch(err => console.error(err));

    }
};

const mutations = {
    ADD_STATE: (state, { qId, qState }) => {
        Vue.set(state, qId, qState);
    },
    REMOVE_STATE: (state, { qId }) => {
        Vue.set(state, qId, {});
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};