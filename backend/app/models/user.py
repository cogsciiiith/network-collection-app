from datetime import datetime

from typing import List, Dict, Union, Optional
from .base import Base, ObjectID, QuestionnaireEnum, ResponseBase

from bson.objectid import ObjectId


class State(Base):
    state: Dict[str, Union[ObjectID, Dict[ObjectID, List[str]]]] = dict()


class StateInResponse(ResponseBase):
    """Each questionnaire state"""
    data: State


class StatesInResponse(ResponseBase):
    """The last saved state for a given user"""
    data: List[State] = list()


class Audit(Base):
    """Audit log entry"""

    state: Dict[str, ObjectID]
    questionnaire_id: str
    username: str
    timestamp: datetime = datetime.utcnow()

class Token(Base):
    token: str

class TokenInResponse(ResponseBase):
    data: Token